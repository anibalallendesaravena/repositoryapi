class pedido{
    constructor(numero, fecha, usurid, codmediodepago, direcenvio, detalle){
        this.numero = numero;
        this.fecha = fecha;
        this.usurid = usurid;
        this.codmediodepago = codmediodepago;
        this.direcenvio = direcenvio;
        this.estado = 'pendiente';
        this. detalle = detalle;
    }
    setEstado(estado){
        this.estado = estado;
    }
}
class detalledelpedido {
    constructor(codproducto, cantidad){
        this.codproducto = codproducto;
        this.cantidad = cantidad;
    }
}

module.exports = pedido;