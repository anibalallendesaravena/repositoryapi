const express = require('express');
const swaggerJSDoc = require('swagger-jsDoc');
const swaggerJsDoc = require('swagger-jsDoc');
const swaggerUI = require('swagger-ui-express');
const server = express();
const logger = require('./libreria/appmind');
const { json } = require('express');
const usuario = require('./libreria/usuario');
const Pedido = require('./libreria/pedido');
const chekeo = require('./libreria/mi');
const estructuras = require('./libreria/estructuras');
const puerto = 6655;

const swaggerOptions = {
    swaggerDefinition: {
        info: {
            title: 'una nueva esperanza',
            version: '1.0.0',
            description: 'pesecillo'
        }
    },
    apis:['./apirest.js']
}

const swaggerDocs = swaggerJSDoc(swaggerOptions);
server.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerDocs));

server.use(express.json());
server.use(logger);

let usuarioAdm = new usuario( 'admin', '', 'admin@mail.com', '', 'admin', 'admin123');
usuarioAdm.setAdmin();
estructuras.usuarios.push(usuarioAdm);


/**
 * @swagger
 * /usuario:
 *  post:
 *    summary: "crear usuario"
 *    description: permite crear una cuenta 
 *    consumes: 
 *    - "application/json"
 *    parameters:
 *    - name: body
 *      description: nombre de una persona
 *      in: body
 *      required: true
 *      type: string
 *      example: {nombre: string, apellido: string, email: string, telefono: string, id: string, password: string}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: El usuario esta creado
 *    
 */

server.post('/usuario', chekeo.validanuevousuario, (req, res) => {

    let usua = new usuario (
        req.body.nombre,
        req.body.apellido,
        req.body.email,
        req.body.telefono,
        req.body.id,
        req.body.password
    );

    estructuras.usuarios.push(usua);
    console.log(`despues: ${JSON.stringify(estructuras.usuarios)}`);

    res.send('usuario creado');
});

/**
 * @swagger
 * /login:
 *  post:
 *    summary: login del usuario
 *    description: permite iniciar la secion al usuario
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: body
 *      description: iniciamos la secion ok
 *      in: body
 *      required: true
 *      type: string
 *      example: {id: String, password: String}
 *    produce:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: ID login
 *      401:
 *        description: El password es incorreto
 *      405:
 *        description: El usuario no fue encontrado
 */


server.post('/login', (req, res) => {

    let lousu = req.body;

    let usuarioencontrado = estructuras.usuarios.find((usu) => usu.id === lousu.id);
    if(!usuarioencontrado){
        usuarioencontrado = estructuras.usuarios.find((usu) => usu.email === lousu.id);
    }
    console.log(usuarioencontrado);

    if(usuarioencontrado){
        if(usuarioencontrado.password === lousu.password){
            let it = estructuras.usuarios.indexOf(usuarioencontrado);
            res.send(`${it}`);
        }else{
            res.status(401).send('password incorrecto');
        }
    }else{
        res.status(404).send('usuario no no fue encontrado');
    }


});

/**
 * @swagger
 * /pedido:
 *  post:
 *    summary: agregar nuevo pedido
 *    descrption: permite crear al usuaio un nuevo pedido
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: sesionid
 *      description: ID devuelto por login
 *      in: header
 *      required: true
 *      type: number
 *    - name: body
 *      description: el cuerpo del pedido
 *      in: body
 *      required: true
 *      type: string
 *      example: {codmediodepago: String, detalle: {codproducto: String, cantidad: Number}}
 *    produces: 
 *    - "applicatio/json"
 *    responses:
 *      200: 
 *        description: la peticon fue exitosa
 */

server.post('/pedido', chekeo.validasesion, chekeo.validadetallepedido, (req, res) => {

    const elped = req.body;
    const fecha = Date();
    const usuheader = estructuras.usuarios[req.header('sesionid')].id;

    estructuras.numpedido = estructuras.numpedido+1;

    let pedi = new Pedido(estructuras.numpedido, fecha, usuheader, elped.codmediodepago, elped.direcenvio, elped.detalle);

    estructuras.pedidos.push(ped);
    console.log(`despues: ${JSON.stringify(estructuras.pedidos)}`);

    res.send('el pedido fue creado');

});

/**
 * @swagger
 * /pedido:
 *  put:
 *    summary: actualizar pedidos
 *    description: se puede actualizar un pedido  un usuario
 *    consumes:
 *    - "application/json"
 *    parameters:
 *    - name: sesionid
 *      description: ID de la sesion devuelta por login
 *      in: header
 *      required: true
 *      type: number
 *    - name: body
 *      description: cuerpo del pedido
 *      in: body
 *      required: true
 *      type: string
 *      example: {numero: Number, codmediodepago: String, detalle: {codproducto: String, direcenvio: string, cantidad: Number}}
 *    produces:
 *    - "application/json"
 *    responses:
 *      200:
 *        description: peticion exitosa
 *      404:
 *        description: pedido no encontrado
 *      406:
 *        description: el pedido esta pendiente  
 */

server.put('/pedido', chekeo.validasesion, chekeo.validamodificapeidido, chekeo.validadetallepedido, (req, res) => {

    const elped = req.body;
    const usuheader = estructuras.usuarios[req.header('sesionid')].id;
    let numpedido = Number.parseInt(elped.numero);
    const index = estructuras.pedidos.findIndex((ped) => ped.numero === numpedido);
    
    estructuras.pedidos[index].codmediodepago = elped.codmediodepago;
    estructuras.pedidos[index].direcenvio = elped.direcenvio;
    estructuras.pedidos[index].detalle = elped.detalle;

    console.log(`despues: ${JSON.stringify(estructuras.pedidos)}`);
    res.send('la peticion fue exitosa');
    
    });

    /**
     * @swagger
     * /mispedidos:
     *  get:
     *  summary: lista de pedidos de los usuarios
     *  description: obtener un listado de pedidos del usuario
     *  parameters:
     *  - name: sesionid
     *    description: ID de sesion devuelta por login
     *    id: header
     *    required: true
     *    type: number
     *  produces:
     *  - "application/json"
     *  responses:
     *    200:
     *      description: peticion exitosa
     *    404:
     *      description: usuario no fue encontrado 
     */

    server.get('/mispedidos', chekeo.validasesion, (req, res) => {

        const usuheader = estructuras.usuarios[req.header('sesionid')];
        res.send(JSON.stringify(encontrado));

    });

    /**
     * @swagger
     * /pedidos:
     *  get:
     *    summary: todos los pedidos
     *    description: poder ver todos los pedidos, solo los admin puede invocar
     *    parameters:
     *    - name: sesionid
     *      description: id de sesion devuelta por login
     *      in: header
     *      required: true
     *      type: number
     *    produces:
     *    - "application/json"
     *    responses:
     *      200:
     *        description: la peticion fue exitosa
     *      401:
     *        description: el usario no es admin
     *      404:
     *        description: el usuario no existe
     */

    server.get('/pedidos', chekeo.validasesion, chekeo.validausuarioadmin, (req, res) =>{
        res.send(JSON.stringify(estructuras.pedidos));
    });

    /**
     * @swagger
     * /pedido/estado:
     *  put:
     *    summary: actualiza el estado del pedido
     *    description: solo los usuarios admin pueden cambiar el estado del pedido
     *    consumes:
     *    - "application/json"
     *    parameters:
     *    - name: sesionid
     *      description: id de sesion devuelto por login
     *      in: header
     *      required: true
     *      type: number
     *    - name: body
     *      description: estado nueco [pendiente, confirmado, en preparacion, enviado, entregado]
     *      in: body
     *      required: true
     *      type: string
     *      example: {numero: numero, estado: string}
     *    produces:
     *    - "application/json"
     *    responses:
     *      200:
     *        description: peticion exitosa
     *      404:
     *        description: pedido no encontrado
     */

    server.put('/pedido/estado', chekeo.validasesion, chekeo.validausuarioadmin, chekeo.validamodificapeidido, (req, res) => {
        
        let pedidoup = req.body;
        console.log(pedidoup);

        let index = estructuras.pedidos.findIndex((ped) => ped.numero === pedidoup.numero);

        estructuras.pedidos[index].estado = pedidoup.estado;
        res.send('peticion exitosa');
    });

    /**
     * @swagger
     * /producto:
     *  post:
     *    summary: crear un producto
     *    description: permite crea un producto solo a los usuarios admin
     *    consumes:
     *    - "application/json"
     *    parameters:
     *    - name: sesionid
     *      description: id devuelto por login
     *      in: header
     *      required: true
     *      type: number
     *    - name:  body
     *      description: curpo del producto
     *      in: body
     *      required: true
     *      type: string
     *      example: {codproducto: string, descripcion: string, precio: number}
     *    produces:
     *    - "application/json"
     *    responses:
     *      200:
     *        descriptio: el producto fue creaado
     *      405:
     *        description: codigo existente
     */

    server.post('/producto', chekeo.validasesion, chekeo.validausuarioadmin, (req, res) => {
        
        const elprod = req.body;

        if(estructuras.productos.find((prd) => prd.codproducto === elprod.codproducto)){
            res.status(405).send('codigo existente');
        }else{
            estructuras.productos.push(elprod);
            console.log(`despues:, ${JSON.stringify(estructuras.productos)}`);
            
            res.send('el producto fue creado');
            }
    });

    /**
     * @swagger
     * /producto:
     *  put:
     *    summary: actualizar el producto
     *    descriptio: permite solo l usuario admin editar un producto
     *    consumes:
     *    - "application/json"
     *    parameters:
     *    - name: sesionid
     *      description: id de sesion devuelta por login
     *      in: header
     *      required: true
     *      type: number
     *    - name: body
     *      description: cuerpo del producto
     *      in: body
     *      required: true
     *      type: string
     *      example: {codproducto: string, descripcion: string, precio: number}
     *    produces:
     *    - "application/json"
     *    responses:
     *      200:
     *        description: la peticio fue exitosa
     *      404:
     *        description: el codigo no fue encontrado  
     */

    server.put('/producto', chekeo.validasesion, chekeo.validausuarioadmin, (req, res) => {

        const elprod = req.body;
       
        let index = estructuras.productos.findIndex((prod) => prod.codproducto === elprod.codproducto);

        if(index > -1){
            estructuras.productos[index].descripcion = elprod.descripcion;
            estructuras.productos[index].precio = elprod.precio;

            console.log(`despues:, ${JSON.stringify(estructuras.productos)}`);
            res.send('peticion exitosa');
        }else{
            res.status(404).send('codigo no fue encontrado');
        }
        
        
        });

        /**
         * @swagger
         * /producto:
         *  delete:
         *    summary: borrar un producto
         *    description: permite eliminar un producto, solo a usuarios que son admin
         *    consumes:
         *    - "application/json"
         *    parameters:
         *    - name: sesionid
         *      description: id de sesion devueto por login
         *      in: header
         *      required: true
         *      type: number
         *    - name: body
         *      description: cuerpo de un producto
         *      in: body
         *      required: true
         *      type: string
         *      example: {codproducto: string}
         *    produces:
         *    - "application/json"
         *    responses:
         *      200:
         *        description: la peticion exitosa
         *      404:
         *        description: codigo no encnotrado
         */

        server.delete('/producto', chekeo.validasesion, chekeo.validausuarioadmin, (req, res) => {

            const elprod = req.body;

            let index = estructuras.productos.findIndex((prod) => prod.codproducto === elprod.codproducto);
            console.log(index);

            if(index > -1){
                estructuras.productos.splice(index, 1);
                console.log(`despues:, ${JSON.stringify(estructuras.productos)}`);

                res.send('peticion exitosaa');
            }else{
                res.status(404).send('el codigo no fue encontrado')
            }
        });

        /**
         * @swagger
         * /productos:
         *  get:
         *    summary: lista de todos los productos
         *    description: optener un listado de todos los productos, solo los usuarios admin lo pueden invocar
         *    parameters:
         *    - name: sesionid
         *      description: id devuelto por login
         *      in: header
         *      required: true
         *      type: number
         *    produces:
         *    -  "application/json"
         *    responses:
         *      200:
         *        description: peticion exitosa  
         */

        server.get('/productos', chekeo.validasesion, chekeo.validausuarioadmin, (req, res) => {

            res.send(JSON.stringify(estructuras.productos));
        });

        /**
         * @swagger
         * /mediosdepago:
         *  post:
         *    summary: crear un medio de pago
         *    description: permite crear un medio de pago, solo a los usuairos admin
         *    consumes:
         *    - "application/json"
         *    parameters:
         *    - name: sesionid
         *      description: id devuelto por login
         *      in: header
         *      required: true
         *      type: number
         *    - name: body
         *      description: cuerpo de un medio de pago
         *      in: body
         *      required: true
         *      type: string
         *      example: {codmediodepago: string, descripcion: string}
         *    produces:
         *    - "application/json"
         *    responses:
         *      200:
         *        description: medio de pago creado
         *      405:
         *        description: codigo existente
         */

        server.post('/mediosdepago', chekeo.validasesion, chekeo.validausuarioadmin, (req, res) => {

            const elmedi = req.body;

            if(estructuras.mediospagos.find((med) => med.codmediodepago === elmedi.codmediodepago)){
                res.status(405).send('codigo existente');
            }else{
                estructuras.mediospagos.push(elmedi);
                console.log(`despues:, ${JSON.stringify(estructuras.mediospagos)}`);

                res.send('medio de pago creado');
            }
            
        });

        /**
         * @swagger
         * /mediosdepago:
         *  put:
         *    summary: actualizr medio de pago
         *    description: permite editar un medio de pago, solo para usuarios admin
         *    consumes: 
         *    - "application/json"
         *    parameters:
         *    - name: sesionid
         *      description: id devuelto por login
         *      id: header
         *      required: true
         *      type: number
         *    - name: body
         *      description: cuerpo de un medio de pago
         *      in: body
         *      required: true
         *      type: string
         *      example: {codmediodepago: sting, descripcion: string}
         *    produces:
         *    - "application/json"
         *    responses:
         *      200:
         *        description: la peticion fue exitosa
         *      404:
         *        description: codigo no encontrado 
         */

        server.put('/mediosdepago', chekeo.validasesion, chekeo.validausuarioadmin, (req, res) => {

            const elmedi = req.body;

            let index = estructuras.mediospagos.findIndex((med) => med.codmediodepago === elmedi.codmediodepago);

            if(index > -1){
                estructuras.mediospagos[index].descripcion = elmedi.descripcion;
                console.log(`despues: ${JSON.stringify(estructuras.mediospagos)}`);

                res.send('peticion exitosa');
            }else{
                res.status(404).send('codigo no encontrado');
            }


        });

        /**
         * @swagger
         * /mediosdepago:
         *  delete:
         *    summary: elimina un medio de pago
         *    description: permite elimina un medio de pago, solo para usuarios admin
         *    consumes:
         *    - "application/json"
         *    parameters:
         *    - name: sesionid
         *      description: id devuelto por login
         *      in: heder
         *      required: true
         *      type: number
         *    - name: body
         *      description: cuerpo de un medio de pago
         *      in: body
         *      required: true
         *      type: string
         *      example: {codmediodepago: string}  
         *    produces:
         *    - "application/json"
         *    responses:
         *      200: 
         *        description: la peticion fue exitosa
         *      404: 
         *        description: codigo no encontrado
         */

        server.delete('/mediosdepago', chekeo.validasesion, chekeo.validausuarioadmin, (req, res) => {

            const elmedi = req.body;

            let index = estructuras.mediospagos.findIndex((med) => med.codmediodepago === elmedi.codmediodepago);
            console.log(index);

            if(index > -1){
                estructuras.mediospagos.splice(index, 1);
                console.log(`despues: ${JSON.stringify(estructuras.mediospagos)}`);

                res.send('peticion exitosa');
            }else{
                res.status(404).send('codigo no encontrado');
            }
        });

        /**
         * @swagger
         * /mediosdepago:
         *  get:
         *    summary: lista de los medios de pagos
         *    description: obtener lista de los medios de pagos, solo para usuiaros admin
         *    parameters:
         *    - name: sesionid
         *      description: id de sesion devuelta por login
         *      in: header
         *      required: true
         *      type: number
         *    produces:
         *    - "application/json"
         *    responses:
         *      200:
         *        description: peticion exitosa
         */

        server.get('/mediosdepago', chekeo.validasesion, chekeo.validausuarioadmin, (req, res) => {

            res.send(JSON.stringify(estructuras.mediospagos));
        
        });








server.listen(puerto, () => {
    console.log(`very good in ${puerto}`);
});
